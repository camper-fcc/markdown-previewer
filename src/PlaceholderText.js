/*
  This file is part of @camper-fcc's Markdown Previewer.

  @camper-fcc's Markdown Previewer is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Markdown Previewer is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Markdown Previewer.  If not,
  see <https://www.gnu.org/licenses/>.
*/

export default `# Welcome to React Markdown Previewer!
[@camper-fCC](https://freecodecamp.org/camper-fcc)

## Fun with React!

This is for the [Build a Markdown Previewer](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-markdown-previewer) challenge.

## :star2: :star2: :star2: :star2: :star2: :star2: :star2:

### Here's some info about the markdown previewer:

Inline code, \`test()\`, is placed between two backticks.

Multi-line code is placed between three backticks (\`\`\`) like this:

\`\`\`
// multi-line code:
const newFunction = data => {
  const test = [];
  return data.map(item => test.push(item.amount));
};
\`\`\`

Multi-line code or code blocks have syntax highlighting, either automatically like above or as specified by the user like:

\`\`\`ruby
class Period < ActiveRecord::Base
  has_many :courses
  default_scope -> { order(start_time: :asc) }
  validates :name, presence: true, uniqueness: { case_sensitive: false,
                                   message: "has already been added." }
  validates :start_time, presence: true, uniqueness: true
  validates :end_time, presence: true, uniqueness: true
  
  # a standard way of displaying the timing of the period
  def duration_text
    start_time.strftime("%l:%M%P") + " - " + end_time.strftime("%l:%M%P")
  end
end
\`\`\`

Text can be **bold** and _italic_ and :arrow_right: :arrow_right: **_both italic and bold_**.

~~Strikethrough~~ is here too, along with emojis! :fireworks: :clap:

:hamburger: :fries: :chicken: :rabbit: :cat: :cat2: :sweat_smile: :smiley: :bike: :airplane: :train: :bus: :cake:

There's also [links](https://www.freecodecamp.com/) as I included above.

> Blockquotes can be single line
or multiple line
like this.

or like this:

> Line 1
> Line 2


Tables:

Align Right | Center | Align Left
:-|:-:|-:
type1 | characteristic | more
type2 | something | less


Unordered Lists:

- Item 1
- Item 2
  - SubItem 1
  - SubItem 2
    - Sub-SubItem 1
    - Sub-SubItem 2
      - Sub-Sub-SubItem 1
      - Sub-Sub-SubItem 2


Ordered Lists:

1. Item 1
  1. SubItem 1
    - Sub-SubItem 1
    - Sub-SubItem 2
  1. SubItem 2
1. Lists continue numbering if there's no line break
1. Item 3
- They keep numbering even if numbers aren't used
* Item 5

Image:

![Sun rising over hills](https://farm7.staticflickr.com/6203/6123290927_0981952009.jpg)

:sunny:

`;

/*
  This file is part of @camper-fcc's Markdown Previewer.

  @camper-fcc's Markdown Previewer is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Markdown Previewer is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Markdown Previewer.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const Header = styled.header`
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 5;
  background-color: white;
  display: flex;
  justify-content: space-between;
`;

const H1 = styled.h1`
  margin: 0;
  font-size: 2rem;
  padding-left: 10px;
`;

const Contact = styled.div`
  padding: 10px;
  display: flex;
  flex-wrap: wrap;

  a {
    padding: 0 5px;
  }
`;

export default () => (
  <Header>
    <H1>Markdown Previewer</H1>
    <Contact>
      <a href="https://freecodecamp.org/camper-fcc">@camper</a>
      <a href="https://gitlab.com/camper-fcc/markdown-previewer/">code</a>
    </Contact>
  </Header>
);

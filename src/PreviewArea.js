/*
  This file is part of @camper-fcc's Markdown Previewer.

  @camper-fcc's Markdown Previewer is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Markdown Previewer is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Markdown Previewer.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import styled from "styled-components";
import DOMPurify from "dompurify";
import { emojify } from "node-emoji";
import { highlight, highlightAuto, getLanguage } from "highlight.js";
import marked from "marked";

import "../node_modules/highlight.js/styles/tomorrow-night-eighties.css";

const PreviewAreaStyle = styled.div`
  padding: 10px;
  height: 50%;
  overflow: auto;
  background-color: rgba(255, 255, 255, 1);

  p img {
    max-width: 100%;
  }

  pre {
    overflow: auto;
    padding: 1rem;
  }

  @media only screen and (min-width: 540px) {
    height: 100%;
    max-width: 50vw;
    padding-top: 0;
  }
`;

const renderer = new marked.Renderer();
renderer.link = (href, title, text) =>
  `<a target="_blank" rel="nofollow" href="${href}">${text}</a>`;

export default class PreviewArea extends Component {
  html() {
    return marked(this.props.inputText, {
      sanitize: true,
      gfm: true,
      breaks: true,
      renderer: renderer,
      highlight: (code, language) =>
        !!(language && getLanguage(language))
          ? highlight(language, code).value
          : highlightAuto(code).value,
      tables: true
    });
  }

  render() {
    return (
      <PreviewAreaStyle
        id="preview"
        dangerouslySetInnerHTML={{
          __html: DOMPurify.sanitize(emojify(this.html()), {
            ADD_ATTR: ["target"]
          })
        }}
      />
    );
  }
}

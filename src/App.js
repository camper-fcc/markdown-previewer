/*
  This file is part of @camper-fcc's Markdown Previewer.

  @camper-fcc's Markdown Previewer is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Markdown Previewer is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Markdown Previewer.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import styled, { createGlobalStyle } from "styled-components";
import Header from "./Header";
import Main from "./Main";

const GlobalStyle = createGlobalStyle`
  *, *:before, *:after {
    box-sizing: border-box;
  }

  body {
    margin: 0;
  }
`;

const Container = styled.div``;

export default class Index extends Component {
  render() {
    return (
      <Container>
        <GlobalStyle />
        <Header />
        <Main />
      </Container>
    );
  }
}

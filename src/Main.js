/*
  This file is part of @camper-fcc's Markdown Previewer.

  @camper-fcc's Markdown Previewer is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Markdown Previewer is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Markdown Previewer.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import styled from "styled-components";

import PlaceholderText from "./PlaceholderText";
import Textarea from "./Textarea";
import PreviewArea from "./PreviewArea";

const MainContainer = styled.main`
  padding-top: 80px;
  height: calc(100vh - 2px);
  background-color: rgba(0, 0, 0, 0.9);

  @media only screen and (min-width: 540px) {
    display: flex;
    padding-top: 50px;
  }
`;

export default class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputText: PlaceholderText,
      clear: this.props.clear
    };

    this.handleEditorChange = this.handleEditorChange.bind(this);
  }

  handleEditorChange(event) {
    this.setState({
      inputText: event.target.value
    });
  }

  render() {
    return (
      <MainContainer>
        <Textarea
          inputText={this.state.inputText}
          handleEditorChange={this.handleEditorChange}
        />
        <PreviewArea inputText={this.state.inputText} />
      </MainContainer>
    );
  }
}
